
import {app, BrowserWindow, ipcMain} from 'electron' // http://electronjs.org/docs/api
import * as path from 'path' // https://nodejs.org/api/path.html
import * as url from 'url' // https://nodejs.org/api/url.html

let window: null | BrowserWindow = null

// Wait until the app is ready
app.once('ready', () => {
  // Create a new window
  window = new BrowserWindow({
    // Set the initial width to 400px
    width: 800,
    // Set the initial height to 500px
    height: 600,
    // Don't show the window until it ready, this prevents any white flickering
    show: false,
    // Don't allow the window to be resized.
    resizable: true,
    autoHideMenuBar: true
  })

  // Load a URL in the window to the local index.html path

  //let indexFile: string = path.join(__dirname, 'dist/frontend/index.html');
  let indexFile: string = path.join('.', 'dist/frontend/index.html');
  console.log(indexFile);
  window.loadURL(url.format({
    pathname: indexFile,
    protocol: 'file:',
    slashes: true
  }));

  // Show window when page is ready
  window.once('ready-to-show', () => {
    window!.show();
  })
});


ipcMain.on('dummy', (event: any, data: any) => {
  console.log(JSON.stringify(data));
});



let dummyCounter: number = 0;
ipcMain.on('dummyCounter', (event: any, data: any) => {
  console.log('dummyCounter', JSON.stringify(data));

  event.sender.send('dummyCounter', dummyCounter);
  dummyCounter++;
});