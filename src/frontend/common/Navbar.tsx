import * as React from "react";


class Navbar extends React.Component
{
	state = {
		items: [
			{
				text: 'home',
				path: '#',
				childs: [],
				disabled: false,
				active: false
			},
			{
				text: 'dropdown',
				path: '#',
				disabled: false,
				active: false,
				childs: [
					{
						text: 'home',
						path: '#',
						childs: [],
						disabled: false,
						active: false
					},{
						text: 'home',
						path: '#',
						childs: [],
						disabled: true,
						active: false
					}
				]
			}
		]
	}

	render(){
		return (
			<>
				<nav className="navbar navbar-expand-lg navbar-light bg-light">
					<a className="navbar-brand" href="#">Navbar</a>
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span className="navbar-toggler-icon"></span>
					</button>

					<div className="collapse navbar-collapse" id="navbarNav">
						<ul className="navbar-nav mr-auto">
							{
								this.state.items.map(item => {
									if(item.childs && item.childs.length > 0){
										return (
											<li className="nav-item dropdown">
												<a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													{item.text}
												</a>
												<div className="dropdown-menu" aria-labelledby="navbarDropdown">
													{item.childs.map(item => 
														<a className={`dropdown-item ${item.disabled ? 'disabled' : ''} ${item.active ? 'active' : ''}`}
																href={item.path}>
															{item.text}
														</a>)}
												<div className="dropdown-divider"></div>
												</div>
											</li>
										);
									}else{
										return (
											<li className="nav-item active">
												<a className={`nav-link ${item.disabled ? 'disabled' : ''} ${item.active ? 'active' : ''}`}
														href={item.path}>
													{item.text}
												</a>
											</li>
										);
									}
								})
							}
						</ul>
					</div>
				</nav>
			</>
		)
	}
}

export default Navbar;