import {ipcRenderer} from 'electron'
import {EventEmitter} from 'events';


class EventHandler extends EventEmitter
{
    constructor(){
        super()
    }

    register(event: string, callBack: Function): {event: string, cB: Function}
    {
        console.log('Register CB for: ', event);
        ipcRenderer.on(event, callBack);
        return {event, cB: callBack};
    }

    unregister(cb: {event: string, cB: Function}){
        console.log('Unregister CB for: ', event);
        ipcRenderer.removeListener(cb.event, cb.cB);
    }

    request(what: string, query: any) : any
    {
        console.log('Requesting: ', what);
        console.log(query);
        ipcRenderer.send(what, query);
    }
}

let EVENTHANDLER = new EventHandler();

export default EVENTHANDLER;