import * as React from "react";
import * as ReactDOM from "react-dom";
import EVENTHANDLER from './common/Intercon';
import Navbar from './common/Navbar';
import * as ChartJS from 'react-chartjs-2';

export interface HelloProps { compiler: string; framework: string; }

class MyChart extends React.Component {
    timerReload: NodeJS.Timer | null = null;
    state = {
        graph: {
            labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    }

    componentDidMount(){ 
        setInterval(() => {
            let newState = {...this.state};
            newState.graph.datasets[0].data = [Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random()]
            this.setState(newState);
        }, 1000);
    }

    componentWillUnmount(){
        if(this.timerReload) clearInterval(this.timerReload);
    }

    render(){

        return(
            <>
                <ChartJS.Bar data={this.state.graph}/>
            </>
        );
    }
}

// 'HelloProps' describes the shape of props.
// State is never set so we use the '{}' type.
export class Hello extends React.Component<HelloProps, {}> {
    state = {
        counter: 0
    }

    componentDidMount(){
        let listener = EVENTHANDLER.register('dummyCounter', (event: any, data: any) => {
            console.log('Counter: ', data);
            let newState = {...this.state};
            newState.counter = data;
            this.setState(newState);
        });
        this.componentWillUnmount = () => {
            EVENTHANDLER.unregister(listener);
        };
    }

    handlerIncreaseDummyCounter(){
        EVENTHANDLER.request('dummyCounter', {});
    }



    render() {
        return (
        <>
            <Navbar />
            <h1>
                Hello from {this.props.compiler} and {this.props.framework}!
            </h1>
            <table className="table">
                <tbody>
                    <tr>
                        <td>
                            <div className="alert alert-success" role="alert">
                                counter: {this.state.counter}
                            </div>
                        </td>
                        <td>
                            <button type="button" className="btn btn-primary"
                                onClick={() => {this.handlerIncreaseDummyCounter()}}
                            >dummyCounter +</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <MyChart />
        </>);
    }
}




ReactDOM.render(
    <Hello compiler="TypeScript" framework="React" />,
    document.getElementById("example")
);
